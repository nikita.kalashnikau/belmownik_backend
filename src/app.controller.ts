import { Controller, Post, Request, UseGuards } from '@nestjs/common';
import { AuthService } from "./auth/auth.service";
import { LocalAuthGuard } from "./auth/local-auth.guard";
import { ApiBody } from "@nestjs/swagger";
import { CreateUserDto } from "./users/dto/create-user.dto";

@Controller()
export class AppController {
  constructor(
    private authService: AuthService
  ) {
  }


  @UseGuards(LocalAuthGuard)
  @ApiBody({
    type: CreateUserDto
  })
  @Post('auth/login')
  async login(@Request() req) {
    return await this.authService.login(req.user);
  }
}
