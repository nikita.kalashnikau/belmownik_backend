import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity('users')
export class User {
    @PrimaryGeneratedColumn({
        type: 'integer',
        name: 'id'
    })
    id: number;

    @Column({
        type: 'character varying',
        name: 'username'
    })
    username: string;


    @Column({
        type: 'character varying',
        name: 'password'
    })
    password: string;
}
