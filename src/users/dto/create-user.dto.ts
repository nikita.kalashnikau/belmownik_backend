import { ApiProperty } from "@nestjs/swagger";

export class CreateUserDto {
  @ApiProperty({
    type: 'string',
    name: 'username',
  })
  username: string;

  @ApiProperty({
    type: 'string',
    name: 'password',
  })
  password: string;
}
