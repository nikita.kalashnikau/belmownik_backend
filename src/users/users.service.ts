import { HttpException, HttpStatus, Injectable, NotFoundException } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { InjectRepository } from "@nestjs/typeorm";
import { User } from "./entities/user.entity";
import { EntityNotFoundError, Repository } from "typeorm";
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersService {

  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>
  ) {
  }

  private async hashPassword(password: string) {
    const saltOrRounds = 10;
    return await bcrypt.hash(password, saltOrRounds);
  }


  async create(createUserDto: CreateUserDto): Promise<User> {
    const candidate = await this.findOneByUsername(createUserDto.username);

    if (candidate) {
      throw new HttpException(
        'User with this username already exists',
        HttpStatus.BAD_REQUEST
      )
    }
    return await this.userRepository.save(
      this.userRepository.create({
        username: createUserDto.username,
        password: await this.hashPassword(createUserDto.password)
      })
    )
  }

  async findAll(): Promise<User[]> {
    try {
      return await this.userRepository.find();
    } catch (e) {
      throw new HttpException('Something went wrong', HttpStatus.BAD_REQUEST)
    }
  }

  async findOneByUsername(username: string) {
    return await this.userRepository.findOneBy({ username });
  }

  async findOneById(id: number): Promise<User> {
    try {
      return await this.userRepository.findOneByOrFail({ id });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException('User not found', HttpStatus.NOT_FOUND);
      }

      throw new HttpException('Something went wrong', HttpStatus.BAD_REQUEST)
    }
  }

  update(id: number, updateUserDto: UpdateUserDto) {
    return `This action updates a #${ id } user`;
  }

  remove(id: number) {
    return this.userRepository.delete({ id });
  }
}
